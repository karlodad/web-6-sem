function tableRowClick() {
    window.location = this.id;
}
function showError(container, errorMessage) 
{
	container.classList.remove("green");
    container.classList.add("reds");
	container.parentNode.className = 'error';
	let msgElem = document.createElement('span');
	msgElem.className = "error-message";
	msgElem.innerHTML = errorMessage;
	container.parentNode.appendChild(msgElem);
}
function resetError(container) 
{
	container.classList.add("green");
	container.parentNode.className = 'form-group';
	if (container.parentNode.lastChild.className == "error-message") 	
        container.parentNode.removeChild(container.parentNode.lastChild);    
}
window.onload = function() 
{
    let tables = document.getElementsByTagName("table");
    for(let i = 0; i < tables.length; i++) {
        let rows = tables[i].getElementsByTagName("tr");
        for(let j = 0; j < rows.length; j++) {
            if(rows[j].getAttribute("id") !== null) {
                rows[j].onclick = tableRowClick;
            }
        }
    }
    var modal           = document.querySelector("#modal"),
        modalOverlay    = document.querySelector("#modal-overlay"),
        closeButton     = document.querySelector("#close-button"),
        openButton      = document.querySelector("#open-button");
    
    if(modal == undefined || modalOverlay ==undefined || closeButton ==undefined || openButton ==undefined )
        return;

    closeButton.addEventListener("click", function(){
        modal.classList.toggle("closed");
        modalOverlay.classList.toggle("closed");
    });
    
    openButton.addEventListener("click", function(){
        modal.classList.toggle("closed");
        modalOverlay.classList.toggle("closed");
    });


}    
function checkroles(str)
{
    let name = document.getElementById("name");
    let id = document.getElementById("id");
    resetError(name);
    if(name.value.length<4)
    {
        showError(name, "• Поле «Название» должно быть длиною больше 4 символов."+"</br>");	
        return false;
    }
    if(!isCheckField(name.value, id == undefined ? null : id.value, "roles"))
    {
        if(str == "ru")
        {
            showError(name,"• Такая роль уже есть"+"</br>");
        }
        else
        {
            showError(name,"• That role exist"+"</br>");
        }
        return false;
    }
    return true;
}
function checklogin(str)
{
    let email       = document.getElementById("email");
    let password    = document.getElementById("password");
    let exit = false;
    resetError(email);
    resetError(password);
    if(email.value.length < 4)
    {
        if(str == "ru")
            showError(email,"• Поле «Email» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(email,"• Field «Email» has 4 symboles."+"</br>");	
        exit = true;
    }
    if(password.value.length < 4)
    {
        if(str == "ru")
            showError(password,"• Поле «Пароль» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(password,"• Field «Password» has 4 symboles."+"</br>");
        exit = true;
    }
    if(!exit && !isLoginUser(email.value, password.value))
    {
        if(str == "ru")
        {
            showError(email,"• Такого пользователя нет"+"</br>");
            showError(password, "");
        }
        else
        {
            showError(email,"• That user not found"+"</br>");
            showError(password, "");
        }
        exit = true;
    }
    return !exit;
}
function checkusers(str)
{
    let email       = document.getElementById("email");
    let firstname   = document.getElementById("firstname");
    let lastname    = document.getElementById("lastname");
    let password    = document.getElementById("password");
    let role_id     = document.getElementById("role_id");
    let id          = document.getElementById("id");
    let preview     = document.getElementById("preview");
    let exit = false;
    resetError(email);
    resetError(firstname);
    resetError(lastname);
    resetError(password);
    resetError(role_id);
    resetError(preview);
    if(email.value.length < 4)
    {
        if(str == "ru")
            showError(email,"• Поле «Email» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(email,"• Field «Email» has 4 symboles."+"</br>");
        exit = true;
    }
    if(firstname.value.length < 4)
    {
        if(str == "ru")
            showError(firstname,"• Поле «Имя» должно быть длиною больше 4 символов."+"</br>");	
        else
            showError(firstname,"• Field «First name» has 4 symboles."+"</br>");
        exit = true;
    }
    if(lastname.value.length < 4)
    {
        if(str == "ru")
            showError(lastname,"• Поле «Фамилия» должно быть длиною больше 4 символов."+"</br>");	
        else
            showError(lastname,"• Field «Last name» has 4 symboles."+"</br>");
        exit = true;
    }
    if(password.value.length < 4)
    {
        if(str == "ru")
            showError(password,"• Поле «Пароль» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(password,"• Field «Password» has 4 symboles."+"</br>");
        exit = true;
    }
    if(role_id.value == -1)
    {
        if(str == "ru")
            showError(role_id,"• Роль не существует"+"</br>");	
        else
            showError(role_id,"• Role not exist"+"</br>");
        exit = true;
    }
    if(preview.getAttribute("value") == 0)
    {
        if(str == "ru")
            showError(preview,"• Файл не подходит"+"</br>");	
        else
            showError(preview,"• This file bad"+"</br>");
        exit = true;
    }
    if(!exit && !isCheckField(email.value, id == undefined ? null : id.value, "users"))
    {
        if(str == "ru")
        {
            showError(email,"• Пользователь с таким email уже есть"+"</br>");
        }
        else
        {
            showError(email,"• User with that email exist"+"</br>");
        }
        exit = true;
    }
    return !exit;
}
function checkproducts(str)
{
    let category_id = document.getElementById("category_id");
    let name        = document.getElementById("name");
    let maker       = document.getElementById("maker");
    let weight      = document.getElementById("weight");
    let price       = document.getElementById("price");
    let total       = document.getElementById("total");
    let id          = document.getElementById("id");
    let preview     = document.getElementById("preview");
    let exit = false;
    resetError(category_id);
    resetError(name);
    resetError(maker);
    resetError(weight);
    resetError(price);
    resetError(total);
    resetError(preview);
    if(category_id.value == -1)
    {
        if(str == "ru")
            showError(category_id,"Категорий не существует");
        else
            showError(category_id,"Categorys not founds");
        exit = true;
    }
    if(name.value.length < 4)
    {
        if(str == "ru")
            showError(name,"• Поле «Название товара» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(name,"• Field «Name product» has 4 symboles."+"</br>");
        exit = true;
    }
    if(maker.value.length < 4)
    {
        if(str == "ru")
            showError(maker,"• Поле «Издатель» должно быть длиною больше 4 символов."+"</br>");	
        else
            showError(maker,"• Field «Maker» has 4 symboles."+"</br>");	
        exit = true;
    }
    if(parseFloat(weight.value) != weight.value || isNaN(weight.value)|| !isFinite(weight.value) ||parseFloat(weight.value) < 0)
    {
        if(str == "ru")
            showError(weight,"• Поле «Вес» не корректно."+"</br>");
        else
            showError(weight,"• Field «Weight» incorrect."+"</br>");
        exit = true;
    }
    if(parseFloat(price.value) != price.value ||isNaN(price.value) ||!isFinite(price.value)|| parseFloat(price.value) < 0)
    {
        if(str == "ru")
            showError(price,"• Поле «Цена» не корректно."+"</br>");
        else
            showError(price,"• Field «Price» incorrect."+"</br>");
        exit = true;
    }
    if(parseInt(total.value) != total.value || parseInt(total.value) < 0)
    {
        if(str == "ru")
            showError(total,"• Поле «Количество» не корректно."+"</br>");
        else
            showError(total,"• Field «Total» incorrect."+"</br>");
        exit = true;
    }    
    if(preview.getAttribute("value") == 0)
    {
        if(str == "ru")
            showError(preview,"• Файл не подходит"+"</br>");	
        else
            showError(preview,"• This file bad"+"</br>");
        exit = true;
    }
    if(!exit && !isCheckField(name.value, id == undefined ? null : id.value, "products"))
    {
        if(str == "ru")
        {
            showError(name,"• Товар с таким именем уже есть"+"</br>");
        }
        else
        {
            showError(name,"• Product with that name exist"+"</br>");
        }
        exit = true;
    }
    return !exit;
}
function checkdeals(str)
{
    let user_id     = document.getElementById("user_id");
    let product_id  = document.getElementById("product_id");
    let exit = false;
    resetError(user_id);
    resetError(product_id);
    if(product_id.value == -1)
    {
        if(str == "ru")
            showError(product_id,"• Товаров нету."+"</br>");
        else	
            showError(product_id,"• Products not found."+"</br>");
        exit = true;
    }
    if(user_id.value == -1)
    {
        if(str == "ru")
            showError(user_id,"• Пользователей не существует"+"</br>");	
        else
            showError(user_id,"• Users not founds"+"</br>");
        exit = true;
    }
    return !exit;
}
function checkcomments(str)
{
    let user_id     = document.getElementById("user_id");
    let product_id  = document.getElementById("product_id");
    let text        = document.getElementById("commenttext");
    let exit        = false;
    resetError(user_id);
    resetError(product_id);
    resetError(text);
    if(product_id.value == -1)
    {
        if(str == "ru")
            showError(product_id,"• Товаров нету."+"</br>");
        else	
            showError(product_id,"• Products not found."+"</br>");
        exit = true;
    }
    if(user_id.value == -1)
    {
        if(str == "ru")
            showError(user_id,"• Пользователей не существует"+"</br>");	
        else
            showError(user_id,"• Users not founds"+"</br>");	
        exit = true;
    }
    if(text.value == '')
    {
        if(str == "ru")
            showError(text,"• Нужен комментарий"+"</br>");	
        else
            showError(text,"• Comment must be "+"</br>");	
        exit = true;
    }
    return !exit;
}
function checkcategs(str)
{
    let name        = document.getElementById("name");
    let shortlink   = document.getElementById("shortlink");
    let id          = document.getElementById("id");
    let preview     = document.getElementById("preview");
    let exit = false;
    resetError(name);
    resetError(shortlink);
    resetError(preview);
    if(preview.getAttribute("value") == 0)
    {
        if(str == "ru")
            showError(preview,"• Файл не подходит"+"</br>");	
        else
            showError(preview,"• This file bad"+"</br>");
        exit = true;
    }
    if(name.value.length < 4)
    {
        if(str == "ru")
            showError(name,"• Поле «Название категории» должно быть длиною больше 4 символов."+"</br>");
        else
            showError(name,"• Field «Name category» has 4 symboles."+"</br>");
        exit = true;
    }
    if(shortlink.value.length < 3)
    {
        if(str == "ru")
            showError(shortlink,"• Поле «Ссылка» должно быть длиною больше 3 символов."+"</br>");
        else
            showError(shortlink,"• Field «Link» has 3 symboles."+"</br>");
        exit = true;
    }
    if(!exit && !isCheckField(name.value, id == undefined ? null : id.value, "categorys"))
    {
        if(str == "ru")
        {
            showError(name,"• Категори с таким именем уже есть"+"</br>");
        }
        else
        {
            showError(name,"• Category with that name exist"+"</br>");
        }
        exit = true;
    }
    return !exit;
}
function checknews(str)
{
    let name        = document.getElementById("name");
    let id          = document.getElementById("id");
    let preview     = document.getElementById("preview");
    let definition     = document.getElementById("definition");
    let exit = false;
    resetError(name);
    resetError(preview);
    resetError(definition);
    if(preview.getAttribute("value") == 0)
    {
        if(str == "ru")
            showError(preview,"• Файл не подходит"+"</br>");	
        else
            showError(preview,"• This file bad"+"</br>");
        exit = true;
    }
    if(name.value.length < 4)
    {
        if(str == "ru")
            showError(name,"• Поле «Название новости должно быть длиною больше 4 символов."+"</br>");
        else
            showError(name,"• Field «Name news» has 4 symboles."+"</br>");
        exit = true;
    }
    if(definition.value.length < 4)
    {
        if(str == "ru")
            showError(definition,"• Поле Описание должно быть длиною больше 4 символов."+"</br>");
        else
            showError(definition,"• Field «Definition» has 4 symboles."+"</br>");
        exit = true;
    }
    if(!exit && !isCheckField(name.value, id == undefined ? null : id.value, "news"))
    {
        if(str == "ru")
        {
            showError(name,"• Новость с таким названием уже есть"+"</br>");
        }
        else
        {
            showError(name,"• News with that name exist"+"</br>");
        }
        exit = true;
    }
    return !exit;
}
function isLoginUser(email, password)
{
	let request = new XMLHttpRequest();
	let answer=false;
	let uri = "/admin/checkAccountUser"+"?email=";
	uri+=email;
	uri+="&password="+password;
	request.open("POST", uri, false);
	request.onreadystatechange = function()
	{
		if(request.readyState != 4) 		
			return;					
		if(request.status == 200)
		{	
            answer = request.response == 0 ? false : true;			
		} 
	}
	request.send();
	return answer;	
}
function isCheckField(name, id, table)
{	
	let request = new XMLHttpRequest();
    let answer=false;
	let uri = "/admin/checkField" + "?name=" + name + "&table=" + table + "&id=" + id;
	request.open("POST", uri, false);
	request.onreadystatechange = function()
	{
		if(request.readyState != 4) 		
			return;					
		if(request.status == 200)
		{	
            answer = request.response == 1 ? false : true;		
		} 
	}
	request.send();
	return answer;
}