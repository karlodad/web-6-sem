    var input = document.querySelector('#photo');
    var inputH = document.querySelector('#photoH');
    var deletePhoto = document.querySelector('.photo_field_delete');
    var preview = document.querySelector('.preview');
    var lang = document.getElementById('lang').value;
    input.style.display = 'none';
    deletePhoto.style.opacity = 0;
    input.addEventListener('change', updateImageDisplay);
    deletePhoto.addEventListener('click', deleteImageDisplay);
    while(preview.firstChild)  
      preview.removeChild(preview.firstChild);
    var curFile = inputH.value;
    if(!curFile) 
    {
        var list = document.createElement('ol');
        var listItem = document.createElement('li');
        var para = document.createElement('p');
        if(lang == "en")
            para.textContent = 'No file for upload';
        else
            para.textContent = 'Нет файла для загрузки';
        listItem.appendChild(para);
        list.appendChild(listItem);
        preview.appendChild(list);
        preview.setAttribute("value", 1);
    }
    else
    {
        var list = document.createElement('ol');
        preview.appendChild(list);
        var listItem = document.createElement('li');
        var image = document.createElement('img');
        image.src = '/uploads/'+(curFile);
        listItem.appendChild(image);
        list.appendChild(listItem);
        preview.setAttribute("value", 1);
        deletePhoto.style.opacity = 1;
    }
    function deleteImageDisplay() 
    {
        let form = document.querySelector("#form");
        let values = new Array(form.elements.length);
        for (let i = 0; i < form.elements.length; i++)         
            values[i] = form.elements.item(i).value;
        form.reset();  
        for (let i = 0; i < form.elements.length; i++)        
            if(form.elements.item(i).type != 'file')            
                form.elements.item(i).value = values[i];      
        deletePhoto.style.opacity = 0;
        updateImageDisplay();
    }
    function updateImageDisplay() 
    {
        while(preview.firstChild)  
            preview.removeChild(preview.firstChild);  
        var curFiles = input.files;
        if(curFiles.length === 0) 
        {
            var list = document.createElement('ol');
            var listItem = document.createElement('li');
            var para = document.createElement('p');
            if(lang == "en")
                para.textContent = 'No file for upload';
            else
                para.textContent = 'Нет файла для загрузки';
            listItem.appendChild(para);
            list.appendChild(listItem);
            preview.appendChild(list);
            preview.setAttribute("value", 1);
            inputH.value = '';
            deletePhoto.style.opacity = 0;
        } 
        else 
        {
            var list = document.createElement('ol');
            preview.appendChild(list);
            for(var i = 0; i < curFiles.length; i++) 
            {
                var listItem = document.createElement('li');
                if(validFileType(curFiles[i])) 
                {
                    if(returnFileSize(curFiles[i].size))
                    {
                        var image = document.createElement('img');
                        image.src = window.URL.createObjectURL(curFiles[i]);
                        listItem.appendChild(image);
                        preview.setAttribute("value", 1);
                        inputH.value = '';
                        deletePhoto.style.opacity = 1;
                    }
                    else
                    {
                        var para = document.createElement('p');
                        if(lang == "en")
                            para.textContent = 'This file soo big.(Size < 2mb) Update your selection.';
                        else
                            para.textContent = 'Этот файл слижком большой.(Размер < 2мб) Выберите другую картинку.';
                        listItem.appendChild(para);
                        preview.setAttribute("value", 0);
                        inputH.value = '';
                        deletePhoto.style.opacity = 0;
                    }
                }
                else 
                {
                    var para = document.createElement('p');
                    if(lang == "en")
                        para.textContent = 'This file not a valid file type. Update your selection.';
                    else
                        para.textContent = 'Этот файл не проходит контроль типов. Выберите другую картинку.';
                    listItem.appendChild(para);
                    preview.setAttribute("value", 0);
                    inputH.value = '';
                    deletePhoto.style.opacity = 0;
                }
                list.appendChild(listItem);
            }
        }
    }
    var fileTypes = 
    [
        'image/jpeg',
        'image/pjpeg',
        'image/png'
    ]
    function validFileType(file) 
    {
        for(var i = 0; i < fileTypes.length; i++)  
            if(file.type === fileTypes[i])     
            return true;
        return false;
    }
    function returnFileSize(number) 
    {
        if((number/1048576).toFixed(1) > 2)         
            return false;        
        return true;
    }