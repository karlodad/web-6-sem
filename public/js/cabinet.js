
var deletes     = document.querySelector("#delete"),
    modalquest  = document.querySelector("#modalquest"),
    no          = document.querySelector("#no"),
    modelaction = document.querySelector("#modelaction");

no.addEventListener("click", function(){
    modalquest.classList.toggle("closed");
    modelaction.classList.toggle("closed");
});

deletes.addEventListener("click", function(){
    modalquest.classList.toggle("closed");
    modelaction.classList.toggle("closed");
});

function showError(container, errorMessage) 
{
	container.classList.remove("green");
    container.classList.add("reds");
	container.parentNode.className = 'error';
	let msgElem = document.createElement('span');
	msgElem.className = "error-message";
	msgElem.innerHTML = errorMessage;
	container.parentNode.appendChild(msgElem);
}
function resetError(container) 
{
	container.classList.add("green");
	container.parentNode.className = 'form-group';
	if (container.parentNode.lastChild.className == "error-message") 	
        container.parentNode.removeChild(container.parentNode.lastChild);    
}
function checkusers(str)
{
    let firstname   = document.getElementById("user_fname");
    let lastname    = document.getElementById("user_lname");
    let preview     = document.getElementById("preview");
    let exit = false;
    resetError(firstname);
    resetError(lastname);
    resetError(preview);
    if(firstname.value.length < 4)
    {
        if(str == "ru")
            showError(firstname,"• Поле «Имя» должно быть длиною больше 4 символов."+"</br>");	
        else
            showError(firstname,"• Field «First name» has 4 symboles."+"</br>");
        exit = true;
    }
    if(lastname.value.length < 4)
    {
        if(str == "ru")
            showError(lastname,"• Поле «Фамилия» должно быть длиною больше 4 символов."+"</br>");	
        else
            showError(lastname,"• Field «Last name» has 4 symboles."+"</br>");
        exit = true;
    }
    if(preview.getAttribute("value") == 0)
    {
        if(str == "ru")
            showError(preview,"• Файл не подходит"+"</br>");	
        else
            showError(preview,"• This file bad"+"</br>");
        exit = true;
    }
    return !exit;
}