function myFunction() 
{
    let x = document.getElementById("menuItems");
    if (x.className === "megaMenu__links") 
    {
        x.className += " megaMenu__links_show";
        document.getElementById("i").className = "active";
    } 
    else 
    {
        x.className = "megaMenu__links";
        document.getElementById("i").className = "";
    }
}
function initMap()
{
    var uluru = {lat: 55.177608, lng: 30.226735};
    var map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: uluru});
    var marker = new google.maps.Marker({position: uluru, map: map});
}
var slideIndex = 1; 
function plusSlide() 
{
    showSlides(slideIndex += 1);
}
function minusSlide() 
{
    showSlides(slideIndex -= 1);  
}
function currentSlide(n) 
{
    showSlides(slideIndex = n);
}
function showSlides(n) 
{
    var slides = document.getElementsByClassName("slider__item");
    var dots = document.getElementsByClassName("sliderWripperDots__item");
    if(slides.length === 0 || dots.length === 0)
        return;
    if (n > slides.length)     
        slideIndex = 1;    
    if (n < 1)     
        slideIndex = slides.length;    
    for (let i = 0; i < slides.length; i++)     
        slides[i].style.display = "none";    
    for (let i = 0; i < dots.length; i++)     
        dots[i].className = dots[i].className.replace(" active", "");    
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}
window.onload = function()
{
    showSlides(slideIndex);   
}