module.exports = new function() 
{ 
    const randomstring 	= require("randomstring");
    const multer  		= require('multer');
    const langLabels    = require('../main-lang.json');
    const storage       = multer.diskStorage(
        {
            destination: 	(req, file, callback) => callback(null, 'uploads'),
            filename: 	    (req, file, callback) => callback(null, Date.now() + randomstring.generate(50))
        });
    const upload        = multer({ storage : storage});
    this.pagination = (c, m) =>
    {
        var current = c,
            last = m,
            delta = 2,
            left = current - delta,
            right = current + delta + 1,
            range = [],
            rangeWithDots = [],
            l;    
        for (var i = 1; i <= last; i++) 
            if (i == 1 || i == last || i >= left && i < right)
                range.push(i);  
        for (var i of range) 
        {
            if (l)             
                if (i - l === 2)                 
                    rangeWithDots.push(l + 1);                
                else 
                    if (i - l !== 1) 
                        rangeWithDots.push('...');
            rangeWithDots.push(i);
            l = i;
        }    
        return rangeWithDots;
    }     
    this.init = (app, model) => 
    {
        app.get('/', (request, response) => 
        {   
            response.redirect('/index');
        });          
        app.get('/replaceLang/:locale', headerCashe, (request, response) => 
        {        
            if(request.params.locale && (request.params.locale == "ru" || request.params.locale == "en"))            
                request.session.lang = request.params.locale;            
            response.redirect(request.headers['referer'].replace(request.headers['origin'],'')); 
        });
        app.get('/index', headerCashe, (request, response) => 
        {   
            model.productsForSlider((err, slider) => 
            { 
                model.categorysForIndex((err, categorys) => 
                {  
                    model.productsForIndex((err, products) => 
                    {   
                        response.render('main/index',
                        {
                            slider:     slider,
                            products:   products,
                            categorys:  categorys,
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang]
                        });
                    });
                });
            });
        });
        app.get('/search', headerCashe, (request, response) => 
        {   
            if(!request.query.q)
                response.redirect('/products');
            else
            {
                var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
                model.count('products where lower(\'name\') like CONCAT(\'%\',lower(\'' + request.query.q + '\'),\'%\')', (err, count)=>
                {
                    var countPage = count % 8 == 0 ? parseInt(count / 8) : parseInt(count / 8) + 1;
                    if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                    {
                        if(page < 1)
                            response.redirect('/search/?q='+request.query.q);
                        else
                            response.redirect('/search/?q='+request.query.q+'&page=' + countPage);
                    }
                    else
                    {    
                        model.productsFind(request.query.q, page-1, (err, data) => 
                        {  
                            response.render('main/search',
                            {             
                                searchw:            request.query.q,
                                products:           data,
                                datauser:           request.session.user,
                                langName:           request.session.lang,
                                lang:               langLabels[request.session.lang],
                                page:               page,
                                count:              countPage == 0 ? 1 : countPage,
                                arrayList:          this.pagination(page,countPage), 
                            });
                        });
                    }
                });                   
            }
        });
        app.get('/viewing', headerCashe, (request, response) => 
        {   
            response.redirect('/products');
        });  
        app.get('/viewing/:id', headerCashe, (request, response) => 
        {   
            model.productRead(request.params.id, (err, data) => 
            {   
                model.commentsRead(request.params.id, (err, comments) => 
                {    
                    response.render('main/viewing',
                    {             
                        product:    data,
                        comments:   comments,
                        datauser:   request.session.user,
                        langName:   request.session.lang,
                        lang:       langLabels[request.session.lang]
                    });
                });
            });
        });
        app.post('/addComment', headerCashe, (request, response) => 
        {
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.comment.product_id !== undefined
                    &&request.body.comment.user_id !== undefined
                    &&request.body.comment.comment_text !== '') 
                {
                    model.saveComment(request.body.comment, (err, result) => 
                    {
                        if(!err)
                            response.redirect('/viewing/' + request.body.comment.product_id);
                        else
                            response.redirect('/error?error=' + err); 
                    });
                } 
                else            
                    response.redirect('/error?error=' + langLabels[request.session.lang].notAllParam); 
            }                    
        });
        app.get('/news', headerCashe, (request, response) => 
        {   
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('news', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/news');
                    else
                        response.redirect('/news?page=' + countPage);
                }
                else
                {     
                    model.newsAll(page - 1, (err, data) => 
                    {  
                        response.render('main/news',
                        {             
                            news:       data,
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage), 
                        });
                    });
                }
            });
        });  
        app.get('/news/:id', headerCashe, (request, response) => 
        {   
            model.newsRead(request.params.id, (err, data) => 
            {  
                response.render('main/newsO',
                {             
                    news:    data,
                    datauser:   request.session.user,
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            });
        });
        app.get('/products', headerCashe, (request, response) => 
        {   
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            var sort = request.query.sort == undefined ? "name_asc" : request.query.sort;
            model.count('products', (err, count)=>
            {
                var countPage = count % 8 == 0 ? parseInt(count / 8) : parseInt(count / 8) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/products' + '?sort=' + sort);
                    else
                        response.redirect('/products?sort=' + sort + '&page=' + countPage);
                }
                else
                {    
                    model.productsAll(null, page - 1, sort, (err, data) => 
                    {  
                        response.render('main/products',
                        {             
                            category:               langLabels[request.session.lang].allProducts,
                            products:               data,
                            sort:                   sort,
                            datauser:               request.session.user,
                            langName:               request.session.lang,
                            lang:                   langLabels[request.session.lang],
                            page:                   page,
                            count:                  countPage == 0 ? 1 : countPage,
                            arrayList:              this.pagination(page,countPage), 
                        });
                    }); 
                }
            }); 
        });  
        app.get('/products/:categ', headerCashe, (request, response) => 
        {   
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            var sort = request.query.sort == undefined ? "name_asc" : request.query.sort;
            model.count('products where category_id = (select id from categorys where shortlink = \''+request.params.categ+'\')', (err, count)=>
            {
                var countPage = count % 8 == 0 ? parseInt(count / 8) : parseInt(count / 8) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/products/'+request.params.categ + '?sort=' + sort);
                    else
                        response.redirect('/products/'+request.params.categ + '?sort=' + sort +'&page=' + countPage);
                }
                else
                {    
                    model.productsAll(request.params.categ, page - 1, sort, (err, data) => 
                    {  
                        model.categorysName(request.params.categ, (err, name) => 
                        { 
                            response.render('main/products',
                            {             
                                category:               name.name,
                                category_definition:    name.definition,
                                products:               data,
                                sort:                   sort,
                                datauser:               request.session.user,
                                langName:               request.session.lang,
                                lang:                   langLabels[request.session.lang],
                                page:                   page,
                                count:                  countPage == 0 ? 1 : countPage,
                                arrayList:              this.pagination(page,countPage), 
                            });
                        });
                    }); 
                }
            });            
        });  
        app.get('/catalog', headerCashe, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('categorys', (err, count)=>
            {
                var countPage = count % 8 == 0 ? parseInt(count / 8) : parseInt(count / 8) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/catalog');
                    else
                        response.redirect('/catalog?page=' + countPage);
                }
                else
                {     
                    model.categorys(page - 1, (err, data) => 
                    {    
                        response.render('main/catalog',
                        {             
                            categorys:  data,    
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:                   page,
                            count:                  countPage == 0 ? 1 : countPage,
                            arrayList:              this.pagination(page,countPage),
                        });
                    });
                }
            });
        }); 
        app.get('/contacts', headerCashe, (request, response) => 
        {   
            response.render('main/contacts',
            {                 
                datauser:   request.session.user,
                langName:   request.session.lang,
                lang:       langLabels[request.session.lang]
            });
        });  
        app.get('/cabinet', headerCashe, (request, response) => 
        {             
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                model.loadDataUserCabinet(request.session.user.id, (err, user) => 
                { 
                    model.loadOrdersUserCabinet(request.session.user.id, (err, orders) => 
                    { 
                        response.render('main/cabinet', 
                        {
                            user:       user, 
                            orders:     orders,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang]
                        });
                    });
                }); 
            }
        });  
        app.get('/about', headerCashe, (request, response) => 
        {   
            response.render('main/about',
            {                 
                datauser:   request.session.user,
                langName:   request.session.lang,
                lang:       langLabels[request.session.lang]
            });
        });
        app.get('/error', headerCashe, (request, response) => 
        {        
            if(request.query.error == undefined)
                response.redirect('/'); 
            response.render('main/error', 
            {
                error:      request.query.error,                
                datauser:   request.session.user,
                langName:   request.session.lang,
                lang:       langLabels[request.session.lang]
            });
        });
        app.get('/registration', headerCashe, (request, response) => 
        {   
            if(request.session.user)
                response.redirect('/index');
            else
            {
                response.render('main/registration',
                { 
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            }
        });      
        app.post('/registration', headerCashe, (request, response) => 
        {   
            if(request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.user.lname.length>4
                    &&request.body.user.fname.length>4
                    &&request.body.user.email.length>4
                    &&request.body.user.password.length>4)
                model.createUser(request.body.user, (err, orders) => 
                {                     
                    if(!err)
                        response.redirect('/login');
                    else
                        response.redirect('/error?error=' + langLabels[request.session.lang].exceptionLoginExist); 
                });
                else
                    response.redirect('/error?error=' + langLabels[request.session.lang].exceptionDataReg); 
            }
        });               
        app.post('/editUserMain', headerCashe, upload.single('user[photo]'), (request, response) => 
        {   
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.user.lname
                    &&request.body.user.fname)
                {
                    if(request.file)
                        request.body.user.photo = request.file.filename;           
                    else                
                        request.body.user.photo = null;
                    console.log(request.body.user);
                    model.editUser(request.body.user, (err, orders) => 
                    {                     
                        response.redirect('/cabinet');
                    });
                }
                else
                    response.redirect('/error?error=' + langLabels[request.session.lang].exceptionEditName); 
            }
        });               
        app.post('/deleteUser', headerCashe, (request, response) => 
        {   
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.user.id)
                {
                    model.deleteUser(request.body.user, (err, orders) => 
                    {                    
                        request.session.destroy(); 
                        response.redirect('/cabinet');
                    });
                }
                else
                    response.redirect('/error?error=' + langLabels[request.session.lang].exceptionDelete); 
            }
        });    
        app.post('/editPasswordUser', headerCashe, (request, response) => 
        {   
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.user.oldPassword!=undefined
                    &&request.body.user.newPassword!=undefined)
                {
                    model.checkPassUser(request.body.user, (err, result) => 
                    {         
                        if(result[0].count==1)
                        {           
                            model.editPassUser(request.body.user, (err, result) => 
                            {                     
                                response.redirect('/cabinet');
                            });
                        }
                        else
                            response.redirect('/error?error=' + langLabels[request.session.lang].exceptionEditPass1); 
                    });
                }
                else
                    response.redirect('/error?error=' + langLabels[request.session.lang].exceptionEditPass2); 
            }
        });     
        app.post('/buyProduct', headerCashe, (request, response) => 
        {   
            if(!request.session.user)
                response.redirect('/index');
            else
            {
                if(request.body.product.user_id!=undefined
                    &&request.body.product.product_id!=undefined)
                {
                    model.buyProduct(request.body.product, (err, result) => 
                    {               
                        model.regDeal(request.body.product, (err, result) => 
                        {                     
                            response.redirect('/cabinet');
                        });
                    });
                }
                else
                    response.redirect('/error?error=' + langLabels[request.session.lang].exceptionNotExist); 
            }
        }); 
        app.get('/login', headerCashe, (request, response) => 
        {   
            if(request.session.user)
                response.redirect('/index');
            else
            {
                response.render('main/login',
                { 
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            }
        }); 
        app.post('/login', headerCashe, (request, response) => 
        {   
            if(request.session.user)
                response.redirect('/index');
            else
            {
                model.checkAccountUser(request.body.user, (err, data) => 
                {
                    if(data)
                    {
                        request.session.user=
                        {
                            id: data.id
                        }
                        response.redirect('/cabinet');
                    }
                    else
                    {
                        response.redirect('/error?error=' + langLabels[request.session.lang].exceptionNotUser); 
                    }
                });
            }
        });
        app.get('/logout',  headerCashe, (request, response) =>
        {
            if(request.session.user)
            {
                request.session.destroy();
                response.redirect('/index'); 
            } 
            else
                response.redirect('/index'); 
        });  
        function headerCashe(req, res, next)
        {
            res.header("Cache-Control", "no-cache, no-store, must-revalidate"); 
            res.header("Pragma", "no-cache"); 
            res.header("Expires", 0); 
			if(!req.session.lang)
                req.session.lang = "ru";
            if(req.session.user)
            {
                model.loadDataUser(req.session.user.id, (err, data) =>
                {
                    if(!err)
                    {
                        req.session.user = 
                        {
                            id:     data.id, 
                            name:   data.name, 
                            role:   data.role                          
                        };    
                        next(); 
                    }
                    else
                        res.redirect('/error?error=' + langLabels[request.session.lang].exceptionloadUser + err); 
                });
            }
            else            
                next(); 
        }   
    }
};
