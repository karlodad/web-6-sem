const { Pool } = require('pg');
/*const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
	ssl: true,
});/*/
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'hardware',
    password: '123123',
    port: 5432
});/**/

module.exports = new function() 
{            
    const orderNameProduct =
    {
        'name_asc'      : 'order by name asc',
        'name_desc'     : 'order by name desc',
        'price_asc'     : 'order by price asc',
        'price_desc'    : 'order by price desc',
    };   
	this.count = (table, callback) => 
    {
        this._operation(
            'select count(*) from '+ table,
            null,
            (err, res) => callback(err, parseInt(res[0].count))
        );
    };
    this.createUser = (user, callback) =>
    {
        this._operation(
            'insert into users (firstname, lastname, email, password, role_id) values($1,$2,$3,$4,1)',
            [user.lname, user.fname, user.email, user.password],
            callback
        );        
    };        
    this.editUser = (user, callback) =>
    {
        if(user.photo == "")
            user.photo = null;
        if(user.photoH != '')
            user.photo = user.photoH;
        this._operation(
            'update users set firstname = $1, lastname = $2, city = $3, dateyear = $4, photo = $5 where id = $6',
            [user.fname, user.lname, user.city==""?null:user.city, user.dob==""?null:user.dob, user.photo, user.id],
            callback
        );        
    };           
    this.editPassUser = (user, callback) =>
    {
        this._operation(
            'update users set password = $1 where id = $2 and password = $3',
            [user.newPassword, user.id, user.oldPassword],
            callback
        );        
    };            
    this.deleteUser = (user, callback) =>
    {
        this._operation(
            'delete from users where id = $1',
            [user.id],
            callback
        );        
    };              
    this.buyProduct = (data, callback) =>
    {
        this._operation(
            'update products set total = total - 1 where id = $1',
            [data.product_id],
            callback
        );        
    };               
    this.regDeal = (data, callback) =>
    {
        this._operation(
            'INSERT INTO "deals" ("user_id","product_id","datebuy") VALUES ($1,$2,$3)',
            [data.user_id, data.product_id, new Date()],
            callback
        );        
    };         
    this.checkPassUser = (user, callback) =>
    {
        this._operation(
            'select count(*) from users where id = $1 and password = $2',
            [user.id, user.oldPassword],
            callback
        );        
    };      
    this.productsForSlider = (callback) =>
    {
        this._operation(
            'select (select id from products p1 where p1.category_id = p2.category_id limit 1),'+
            '(select name from products p1 where p1.category_id = p2.category_id limit 1),'+
            '(select price from products p1 where p1.category_id = p2.category_id limit 1),'+
            '(select photo from products p1 where p1.category_id = p2.category_id limit 1) from products p2 group by category_id order by category_id limit 3',
            null,
            callback
        );        
    };         
    this.categorysForIndex = (callback) =>
    {
        this._operation(
            'select * from categorys group by id limit 3',
            null,
            callback
        );        
    };         
    this.productsForIndex = (callback) =>
    {
        this._operation(
            'select id, name, price, photo from products group by id limit 4',
            null,
            callback
        );        
    }; 
    this.productRead = (id, callback) =>
    {
        this._operation(
            'SELECT products.*, categorys.shortlink, categorys.name as namecateg from products join categorys on products.category_id = categorys.id where products.id = $1',
            [id],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };     
    this.commentsRead = (id, callback) =>
    {
        this._operation(
            'SELECT to_char(datewrite, \'YYYY-MM-DD\') as datewrite, photo, CONCAT(lastName , \' \' , firstName) as name, commenttext from comments join users on users.id=comments.user_id where product_id = $1 order by datewrite desc',
            [id],
            callback
        );        
    };    
    this.saveComment = (data, callback) => 
    {
        this._operation(
            'INSERT INTO "comments" ("product_id","user_id","commenttext") VALUES ($1,$2,$3)',
            [data.product_id, data.user_id, data.comment_text],
            callback
        );
        
    };     
    this.productsFind = (q, page, callback) =>
    {
        this._operation(
            'SELECT id, name, price, photo from products where lower(name) like CONCAT(\'%\',lower($1),\'%\') order by name limit 8 offset $2',
            [q, page*8],
            callback
        );        
    }; 
    this.newsAll = (page, callback) =>
    {
        this._operation(
            'SELECT id, name, photo, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, substring(definition, 0 , 100) as definition from news order by datewrite desc limit 5 offset $1',
            [page*5],
            callback
        );
    }; 
    this.newsRead = (id, callback) =>
    {
        this._operation(
            'SELECT id, name, photo, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, definition from news where id = $1',
            [id],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };    
    this.productsAll = (link, page, sort, callback) =>
    {
        if(link)
        {
            this._operation(
                'SELECT id, name, price, photo from products where category_id = (select id from categorys where shortlink = $1) ' + (orderNameProduct[sort] ? orderNameProduct[sort] :'order by name asc') + ' limit 8 offset $2',
                [link, page*8],
                callback
            );
        }
        else
        {
            this._operation(
                'SELECT id, name, price, photo from products ' + (orderNameProduct[sort] ? orderNameProduct[sort] :'order by name asc') + ' limit 8 offset $1',
                [page*8],
                callback
            );
        }
    };    
    this.categorysName = (link, callback) =>
    {
        this._operation(
            'SELECT name, definition, shortlink from categorys where shortlink = $1',
            [link],
            (err, res) => callback(err, res ? res[0] : null)
        );
    }; 
    this.categorys = (page, callback) =>
    {
        this._operation(
            'SELECT *, (select count(*) from products where products.category_id = categorys.id) as countprod from categorys order by name limit 8 offset $1',
            [page*8],
            callback
        );
    }; 
    this.checkAccountUser = (data, callback) =>
    {
        this._operation(
            'SELECT id FROM users where email = $1 and password = $2',
            [data.email, data.password],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };
    this.loadDataUser = (id, callback) =>
    {
        this._operation(
            'SELECT users.id as id, CONCAT(lastName , \' \' , firstName) as name, roles.name as role FROM users inner join roles on roles.id = users.role_id where users.id = $1',
            [id],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };    
    this.loadDataUserCabinet = (id, callback) =>
    {
        this._operation(
            'SELECT lastName, firstName, city, to_char(dateyear, \'YYYY-MM-DD\') as dateyear, email, photo FROM users  where id = $1',
            [id],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };    
    this.loadOrdersUserCabinet = (id, callback) =>
    {
        this._operation(
            'select to_char(datebuy, \'YYYY/MM/DD\') as datebuy, products.name as name, products.price as price from deals JOIN products ON products.id = deals.product_id where deals.user_id = $1 order by datebuy',
            [id],
            callback
        );
    };
    this._operation = (sql, data, callback) => 
    {
        pool.connect((err, client, release) => 
        {
            console.log(`TRYING TO CONNECTION CREATE`);
            if(!err) 
            {
                console.log(`CONNECTION CREATE OK`);
                console.log(`TRYING TO EXECUTE QUERY\n"${sql}"\nWITH DATA\n${JSON.stringify(data)}`);
                client.query(sql, data, (err, res) => 
                {
                    if(!err) 
                    {
                        console.log(`EXECUTE QUERY OK`);
                        callback(err, res ? res.rows : undefined);
                    } 
                    else 
                    {
                        console.log(`EXECUTE QUERY ERROR\n${err}`);
                        callback(err);
                    }
                    console.log(`TRYING TO CONNECTION CLOSE`);
                    release(err => 
                    {
                        if(!err) 
                        {
                            console.log(`CONNECTION CLOSE OK`);
                        } else {
                            console.log(`CONNECTION CLOSE ERROR\n${err}`);
                        }
                    });
                });
            } 
            else 
            {
                console.log(`CONNECTION CREATE ERROR\n${err}`);
                callback(err);
            }
        });
    };
};
