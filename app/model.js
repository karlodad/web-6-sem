const { Pool } = require('pg');
/*const pool = new Pool({
    connectionString: process.env.DATABASE_URL,
	ssl: true,
});*/ 
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'hardware',
    password: '123123',
    port: 5432
});/**/
module.exports = new function() 
{
    const tableFeildForCheck =
    {
        'users'     : 'email',
        'roles'     : 'name',
        'products'  : 'name',
        'news'      : 'name',
        'categorys' : 'name',
        'deals'     : 'datebuy',
        'comments'  : 'datewrite'
    };    
    this.loadDataUser = (id, callback) =>
    {
        this._operation(
            'SELECT users.id as id, CONCAT(lastName , \' \' , firstName) as name, roles.name as role FROM users inner join roles on roles.id = users.role_id where users.id = $1',
            [id],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };  
	this.count = (table, callback) => 
    {
        this._operation(
            'select count(*) from '+ table,
            null,
            (err, res) => callback(err, parseInt(res[0].count))
        );
    };
    this.checkAccountUser = (data, callback) =>
    {
        this._operation(
            'SELECT count(*) FROM users where email = $1 and password = $2',
            [data.email, data.password],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };
    this.checkFieldInTable = (data, callback) =>
    {
        this._operation(
            'SELECT count(*) FROM ' + data.table + ' where ' + tableFeildForCheck[data.table] + ' = $1' + (data.id != 'null' ? (' and id!=' + data.id): ''),
            [data.name],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };
    this.checkUser = (data, callback) =>
    {
        this._operation(
            'SELECT users.id as id, CONCAT(lastName , \' \' , firstName) as name, roles.name as role FROM users inner join roles on roles.id = users.role_id where users.email = $1 and users.password = $2',
            [data.email, data.password],
            (err, res) => callback(err, res ? res[0] : null)
        );
    };
    this.readUsers = (page, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'SELECT users.id, email, firstname, lastname, to_char(dateyear, \'YYYY-MM-DD\') as dateyear, city, photo, roles.name as role_id FROM "users" INNER JOIN roles ON users.role_id=roles.id order by email',
                null,
                callback
            );
        }
        else
        {
            this._operation(
                'SELECT users.id, email, firstname, lastname, to_char(dateyear, \'YYYY-MM-DD\') as dateyear, city, photo, roles.name as role_id FROM "users" INNER JOIN roles ON users.role_id=roles.id order by email limit 5 offset $1',
                [page * 5],
                callback
            ); 
        }
    };    
    this.readUser = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT id, email, firstname, lastname, to_char(dateyear, \'YYYY-MM-DD\') as dateyear, city, photo, password, role_id FROM "users" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };    
    this.saveUser = (data, callback) => 
    {
        if(data.dateyear == "")
            data.dateyear = null;
        if(data.city == "")
            data.city = null;
        if(data.photo == "")
            data.photo = null;
        if(data.photoH != '')
            data.photo = data.photoH;
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "users" SET "email" = $1,"firstname" = $2,"lastname" = $3,"dateyear" = $4,"city" = $5,"photo" = $6,"password" = $7,"role_id" = $8 WHERE "id" = $9',
                [data.email, data.firstname, data.lastname, data.dateyear, data.city, data.photo, data.password, data.role_id, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "users" ("email","firstname","lastname","dateyear","city","photo","password","role_id") VALUES ($1,$2,$3,$4,$5,$6,$7,$8)',
                [data.email, data.firstname, data.lastname, data.dateyear, data.city, data.photo, data.password, data.role_id],
                callback
            );
        }
    };
    this.deleteUser = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "users" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this.readRoles = (page, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'select * from "roles" order by name',
                null,
                callback
            );
        }
        else
        {
            this._operation(
                'select * from "roles" order by name limit 5 offset $1',
                [page * 5],
                callback
            );
        }
    };
    this.readRole = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT *,(select count(*) from users where users.role_id = roles.id) as cu FROM "roles" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };
    this.saveRole = (data, callback) => 
    {
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "roles" SET "name" = $1 WHERE "id" = $2',
                [data.name, data.id],
                callback
            );
        } 
        else 
        {
            this._operation(
                'INSERT INTO "roles" ("name") VALUES ($1)',
                [data.name],
                callback
            );
        }
    };
    this.deleteRole = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "roles" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this.readDeals = (page, id, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'select deals.id as id, users.firstname as user_id, products.name as product_id, to_char(datebuy, \'YYYY-MM-DD\') as datebuy from "deals" INNER JOIN users ON users.id=deals.user_id INNER JOIN products ON products.id=deals.product_id order by datebuy' + (id != null ? ' where users.id = $1' : ""),
                id != null ? [id] : null,
                callback
            );
        }
        else
        {
            this._operation(
                'select deals.id as id, users.firstname as user_id, products.name as product_id, to_char(datebuy, \'YYYY-MM-DD\') as datebuy from "deals" INNER JOIN users ON users.id=deals.user_id INNER JOIN products ON products.id=deals.product_id order by datebuy limit 5 offset $1',
                [page * 5],
                callback
            );
        }
    }; 
    this.readDeal = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT id, user_id, product_id, to_char(datebuy, \'YYYY-MM-DD\') as datebuy  FROM "deals" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };    
    this.saveDeal = (data, callback) => 
    {
        if(data.datebuy == "")
            data.datebuy = new Date();
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "deals" SET "user_id" = $1,"product_id" = $2,"datebuy" = $3 WHERE "id" = $4',
                [data.user_id, data.product_id, data.datebuy, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "deals" ("user_id","product_id","datebuy") VALUES ($1,$2,$3)',
                [data.user_id, data.product_id, data.datebuy],
                callback
            );
        }
    };
    this.deleteDeal = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "deals" WHERE "id" = $1',
            [id],
            callback
        );
    };     
    this.newsAll = (page, callback) =>
    {
        this._operation(
            'SELECT id, name, photo, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, definition from news order by datewrite desc limit 5 offset $1',
            [page * 5],
            callback
        );
    };  
    this.readComments = (page, id, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'select comments.id as id, products.name as product_id, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, users.firstname as user_id, commenttext from "comments" INNER JOIN users ON users.id=comments.user_id INNER JOIN products ON products.id=comments.product_id order by datewrite' + (id!=null?' where users.id=$1':''),
                id != null ? [id] : null,
                callback
            );
        }
        else
        {
            this._operation(
                'select comments.id as id, products.name as product_id, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, users.firstname as user_id, commenttext from "comments" INNER JOIN users ON users.id=comments.user_id INNER JOIN products ON products.id=comments.product_id order by datewrite limit 5 offset $1',
                [page * 5],
                callback
            );
        }
    };       
    this.readNews = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT id, name, photo, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, definition FROM "news" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };   
    this.readComment = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT id, product_id, to_char(datewrite, \'YYYY-MM-DD\') as datewrite, user_id, commenttext FROM "comments" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };     
    this.saveNews = (data, callback) => 
    {
        if(data.photo == "")
            data.photo = null;            
        if(data.photoH != '')
            data.photo = data.photoH;
        if(data.datewrite =="")
            data.datewrite=new Date();
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "news" SET "name" = $1,"photo" = $2,"datewrite" = $3,"definition" = $4 WHERE "id" = $5',
                [data.name, data.photo, data.datewrite, data.definition, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "news" ("name","photo","datewrite","definition") VALUES ($1,$2,$3,$4)',
                [data.name, data.photo, data.datewrite, data.definition],
                callback
            );
        }
    };   
    this.saveComment = (data, callback) => 
    {
        if(data.datewrite =="")
            data.datewrite=new Date();
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "comments" SET "product_id" = $1,"datewrite" = $2,"user_id" = $3,"commenttext" = $4 WHERE "id" = $5',
                [data.product_id, data.datewrite, data.user_id, data.commenttext, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "comments" ("product_id","datewrite","user_id","commenttext") VALUES ($1,$2,$3,$4)',
                [data.product_id, data.datewrite, data.user_id, data.commenttext],
                callback
            );
        }
    };
    this.deleteNews = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "news" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this.deleteComment = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "comments" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this.readCategorys = (page, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'select * from "categorys" order by name',
                null,
                callback
            );
        }
        else
        {
            this._operation(
                'select * from "categorys" order by name limit 5 offset $1',
                [page * 5],
                callback
            );
        }
    };    
    this.readCategory = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT *, (select count(*) from "products" where products.category_id = categorys.id) as cu FROM "categorys" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };    
    this.saveCategory = (data, callback) => 
    {
        if(data.photo == "")
            data.photo = null;            
        if(data.photoH != '')
            data.photo = data.photoH;
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "categorys" SET "name" = $1,"photo" = $2,"shortlink" = $3,"definition" = $4 WHERE "id" = $5',
                [data.name, data.photo, data.shortlink, data.definition, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "categorys" ("name","photo","shortlink","definition") VALUES ($1,$2,$3,$4)',
                [data.name, data.photo, data.shortlink, data.definition],
                callback
            );
        }
    };
    this.deleteCategory = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "categorys" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this.readProducts = (page, callback) => 
    {
        if(page == null)
        {
            this._operation(
                'SELECT products.id, categorys.name as category_id, products.name, maker, weight, price, total, products.definition, products.photo FROM products INNER JOIN categorys ON products.category_id=categorys.id order by categorys.name',
                null,
                callback
            );
        }
        else
        {
            this._operation(
                'SELECT products.id, categorys.name as category_id, products.name, maker, weight, price, total, products.definition, products.photo FROM products INNER JOIN categorys ON products.category_id=categorys.id order by categorys.name limit 5 offset $1',
                [page * 5],
                callback
            );
        }
    };    
    this.readProduct = (id, callback) => 
    {
        if(id) 
        {
            this._operation(
                'SELECT * FROM "products" WHERE "id" = $1',
                [id],
                (err, res) => callback(err, res ? res[0] : null)
            );
        } 
        else 
        {
            callback(null, null);
        }
    };    
    this.saveProduct = (data, callback) => 
    {
        if(data.photo == "")
            data.photo = null;
        if(data.definition == "")
            data.definition = null;            
        if(data.photoH != '')
            data.photo = data.photoH;
        if(data.id !== undefined) 
        {
            this._operation(
                'UPDATE "products" SET "category_id" = $1,"name" = $2, "maker" = $3, "weight" = $4, "price" = $5, "total" = $6, "definition" = $7, "photo" = $8 WHERE "id" = $9',
                [data.category_id, data.name, data.maker, data.weight, data.price, parseInt(data.total), data.definition,data.photo, data.id],
                callback
            );
        }
        else 
        {
            this._operation(
                'INSERT INTO "products" ("category_id", "name", "maker", "weight", "price", "total", "definition", "photo") VALUES ($1,$2,$3,$4,$5,$6,$7,$8)',
                [data.category_id, data.name, data.maker, data.weight, data.price, data.total, data.definition, data.photo],
                callback
            );
        }
    };
    this.deleteProduct = (id, callback) => 
    {
        this._operation(
            'DELETE FROM "products" WHERE "id" = $1',
            [id],
            callback
        );
    };
    this._operation = (sql, data, callback) => 
    {
        pool.connect((err, client, release) => 
        {
            console.log(`TRYING TO CONNECTION CREATE`);
            if(!err) 
            {
                console.log(`CONNECTION CREATE OK`);
                console.log(`TRYING TO EXECUTE QUERY\n"${sql}"\nWITH DATA\n${JSON.stringify(data)}`);
                client.query(sql, data, (err, res) => 
                {
                    if(!err) 
                    {
                        console.log(`EXECUTE QUERY OK`);
                        callback(err, res ? res.rows : undefined);
                    } 
                    else 
                    {
                        console.log(`EXECUTE QUERY ERROR\n${err}`);
                        callback(err);
                    }
                    console.log(`TRYING TO CONNECTION CLOSE`);
                    release(err => 
                    {
                        if(!err) 
                        {
                            console.log(`CONNECTION CLOSE OK`);
                        } else {
                            console.log(`CONNECTION CLOSE ERROR\n${err}`);
                        }
                    });
                });
            } 
            else 
            {
                console.log(`CONNECTION CREATE ERROR\n${err}`);
                callback(err);
            }
        });
    };
};
