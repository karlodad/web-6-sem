module.exports = new function() 
{
    const randomstring 	= require("randomstring");
    const multer  		= require('multer');
    const langLabels    = require('../admin-lang.json');
    const storage       = multer.diskStorage(
    {
        destination: 	(req, file, callback) => callback(null, 'uploads'),
        filename: 	    (req, file, callback) => callback(null, Date.now() + randomstring.generate(50))
    });
    const upload        = multer({ storage : storage});
    this.pagination = (c, m) =>
    {
        var current = c,
            last = m,
            delta = 2,
            left = current - delta,
            right = current + delta + 1,
            range = [],
            rangeWithDots = [],
            l;    
        for (var i = 1; i <= last; i++) 
            if (i == 1 || i == last || i >= left && i < right)
                range.push(i);  
        for (var i of range) 
        {
            if (l)             
                if (i - l === 2)                 
                    rangeWithDots.push(l + 1);                
                else 
                    if (i - l !== 1) 
                        rangeWithDots.push('...');
            rangeWithDots.push(i);
            l = i;
        }    
        return rangeWithDots;
    }      
    this.init = (app, model) => 
    {
        app.post('/admin/checkAccountUser', headerCashe, (request, response) => 
        {        
            if(request.query.email!==undefined && request.query.password!==undefined)
            { 
                model.checkAccountUser(request.query, (err, data) => 
                {
                    response.send(data.count);
                });
            }
        });
        app.post('/admin/checkField', headerCashe, (request, response) => 
        {        
            if(request.query.name!==undefined && request.query.table!==undefined)
            { 
                model.checkFieldInTable(request.query, (err, data) => 
                {
                    response.send(data.count);
                });
            }
        });
        app.post('/admin/replaceLang', headerCashe, (request, response) => 
        {        
            request.session.lang = request.body.lang;
            response.redirect(request.headers['referer'].replace(request.headers['origin'],''));  
        });
        app.get('/admin/login', headerCashe, (request, response) => 
        {        
            if(request.session.user)            
                response.redirect('/admin');            
            else
                response.render('admin/admin_login',
                { 
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
        });
        app.post('/admin/loginUser', headerCashe, (request, response) => 
        {        
            model.checkUser(request.body, (err, data) => 
            {
                if(data)
                {
                    request.session.user = 
                    {
                        id: data.id                        
                    };
                    response.redirect('/admin');    
                } 
                else  
                    response.redirect('/admin/error?error=' + langLabels[request.session.lang].notFoundUser);
            });
        });
        app.get('/admin/error', headerCashe, (request, response) => 
        {        
            if(request.query.error == undefined)
                response.redirect('/admin'); 
            response.render('admin/admin_error', 
            {
                error:      request.query.error,                
                datauser:   request.session.user,
                langName:   request.session.lang,
                lang:       langLabels[request.session.lang]
            });
        });
        app.get('/admin', headerCashe, checkSignIn, (request, response) => 
        {   
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('deals', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/');
                    else
                        response.redirect('/admin?page=' + countPage);
                }
                else
                {  
                    model.readDeals(page - 1, request.session.user.role == "user" ? request.session.user.id : null, (err, data) => 
                    {
                        response.render('admin/admin',
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage),
                        });
                    });
                }
            });
        });    
        app.get('/admin/deals/addDeal', headerCashe, checkSignIn, (request, response) => 
        {
            model.readDeal(request.query.id, (err, data) => 
            {
                if(data === null)  
                {               
                    data = 
                    {
                        product_id:'',
                        datebuy:'',
                        user_id:''
                    };   
                    model.readProducts(null, (err, products) => 
                    {  
                        if(request.session.user.role != "user")
                        {
                            model.readUsers(null, (err, users) => 
                            { 
                                response.render('admin/admin_addDeal', 
                                {
                                    data:       data,
                                    products:   products,
                                    users:      users,                
                                    datauser:   request.session.user,
                                    langName:   request.session.lang,
                                    lang:       langLabels[request.session.lang]
                                });
                            });
                        }
                        else
                        {
                            model.readUser(request.session.user.id,(err, user) => 
                            { 
                                response.render('admin/admin_addDeal', 
                                {
                                    data:       data,
                                    products:   products,
                                    user:       user,                
                                    datauser:   request.session.user,
                                    langName:   request.session.lang,
                                    lang:       langLabels[request.session.lang]
                                });
                            });
                        }
                    });
                }
                else
                {
                    if(data.user_id != request.session.user.id && request.session.user.role =="user")                    
                        response.redirect('/admin/error?error=' + langLabels[request.session.lang].outOfBarrier);                      
                    else
                    {
                        model.readProducts(null, (err, products) => 
                        {  
                            if(request.session.user.role != "user")
                            {
                                model.readUsers(null, (err, users) => 
                                { 
                                    response.render('admin/admin_addDeal', 
                                    {
                                        data:       data,
                                        products:   products,
                                        users:      users,                
                                        datauser:   request.session.user,
                                        langName:   request.session.lang,
                                        lang:       langLabels[request.session.lang]
                                    });
                                });
                            }
                            else
                            {
                                model.readUser(request.session.user.id,(err, user) => 
                                { 
                                    response.render('admin/admin_addDeal', 
                                    {
                                        data:       data,
                                        products:   products,
                                        user:       user,                
                                        datauser:   request.session.user,
                                        langName:   request.session.lang,
                                        lang:       langLabels[request.session.lang]
                                    });
                                });
                            }
                        });
                    }
                }                 
            });
        });
        app.post('/admin/deals/saveDeal', headerCashe, checkSignIn, (request, response) => 
        {
            if(
                request.body.product_id !== undefined && 
                request.body.datebuy !== undefined &&
                request.body.user_id !== undefined) 
            {
                model.saveDeal(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/deals/removeDeal', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.deleteDeal(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        });
        app.get('/admin/users', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('users', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/users');
                    else
                        response.redirect('/admin/users?page=' + countPage);
                }
                else
                {                    
                    model.readUsers(page - 1, (err, data) => 
                    {
                        response.render('admin/admin_users', 
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage),
                        });
                    });
                }
            });
        });        
        app.get('/admin/users/addUser', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.readUser(request.query.id, (err, data) => 
            {
                if(data === null)                 
                    data = 
                    {
                        email:'',
                        firstname:'',
                        lastname:'',
                        dateyear:null,
                        city:null,
                        photo:null,
                        password:'',
                        role_id:''
                    };
                model.readRoles(null, (err, roles) => 
                {            
                    response.render('admin/admin_addUser', 
                    {
                        data:       data,
                        roles:      roles,                
                        datauser:   request.session.user,
                        langName:   request.session.lang,
                        lang:       langLabels[request.session.lang]
                    });
                }); 
            });
        });
        app.post('/admin/users/saveUser', headerCashe, checkSignIn, roleAdmin, upload.single('photo'), (request, response) => 
        {
            if(
                request.body.email !== undefined && 
                request.body.password !== undefined && 
                request.body.firstname !== undefined && 
                request.body.lastname !== undefined) 
            {
                if(request.file)
                    request.body.photo = request.file.filename;           
                else                
                    request.body.photo = null;
                
                model.saveUser(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/users');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/users/removeUser', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.deleteUser(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/users');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        });
        app.get('/admin/roles', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('roles', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/roles');
                    else
                        response.redirect('/admin/roles?page=' + countPage);
                }
                else
                {     
                    model.readRoles(page - 1, (err, data) => 
                    {
                        response.render('admin/admin_role',
                        {                    
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage),
                        });
                    });
                }
            });
        });        
        app.get('/admin/roles/addRole', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.readRole(request.query.id, (err, data) => 
            {
                if(data === null)                 
                    data = 
                    {
                        name:''
                    };                
                response.render('admin/admin_addRole', 
                {
                    data:       data,                
                    datauser:   request.session.user,
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            });
        });
        app.post('/admin/roles/saveRole', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            if(request.body.name !== undefined) 
            {
                model.saveRole(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/roles');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/roles/removeRole', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.deleteRole(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/roles');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        });
        app.get('/admin/prodc', headerCashe, checkSignIn, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('products', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/prodc');
                    else
                        response.redirect('/admin/prodc?page=' + countPage);
                }
                else
                {     
                    model.readProducts(page - 1, (err, data) => 
                    {
                        response.render('admin/admin_product',
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage),
                        });
                    });
                }
            });
        });        
        app.get('/admin/prodc/addProduct', headerCashe, checkSignIn, roleManager, (request, response) => 
        {
            model.readProduct(request.query.id, (err, data) => 
            {
                if(data === null)                 
                    data = 
                    {
                        name:'',
                        maker:'',
                        weight:0,
                        price:0,
                        total:0,
                        definition:null,
                        photo:null
                    };  
                model.readCategorys(null, (err, categs) => 
                {  
                    response.render('admin/admin_addProduct', 
                    {
                        data:       data,
                        categs:     categs,                
                        datauser:   request.session.user,
                        langName:   request.session.lang,
                        lang:       langLabels[request.session.lang]
                    });
                });
            });
        });
        app.post('/admin/prodc/saveProduct', headerCashe, checkSignIn, roleManager, upload.single('photo'), (request, response) => 
        {
            if(
                request.body.name !== undefined && 
                request.body.category_id !== undefined &&
                request.body.maker !== undefined&& 
                request.body.weight !== undefined&& 
                request.body.price !== undefined&& 
                request.body.total !== undefined) 
            {
                if(request.file)
                    request.body.photo = request.file.filename;           
                else                
                    request.body.photo = null;
                model.saveProduct(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/prodc');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/prodc/removeProduct', headerCashe, checkSignIn, roleManager, (request, response) => 
        {
            model.deleteProduct(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/prodc');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        });
        app.get('/admin/commt', headerCashe, checkSignIn, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('comments', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/commt');
                    else
                        response.redirect('/admin/commt?page=' + countPage);
                }
                else
                {     
                    model.readComments(page - 1, request.session.user.role == "user" ? request.session.user.id : null, (err, data) => 
                    {
                        response.render('admin/admin_comments',
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage), 
                        });
                    });
                }
            });
        });        
        app.get('/admin/commt/addComment', headerCashe, checkSignIn, (request, response) => 
        {
            model.readComment(request.query.id, (err, data) => 
            {
                if(data === null)      
                {           
                    data = 
                    {
                        product_id:'',
                        datewrite:'',
                        user_id:'',
                        commenttext:''
                    };                                  
                    model.readProducts(null, (err, products) => 
                    {  
                        if(request.session.user.role != "user")
                        {
                            model.readUsers(null, (err, users) => 
                            { 
                                response.render('admin/admin_addComment', 
                                {
                                    data:       data,
                                    products:   products,
                                    users:      users,                
                                    datauser:   request.session.user,
                                    langName:   request.session.lang,
                                    lang:       langLabels[request.session.lang]
                                });
                            });
                        }
                        else
                        {
                            model.readUser(request.session.user.id, (err, user) => 
                            { 
                                response.render('admin/admin_addComment', 
                                {
                                    data:       data,
                                    products:   products,
                                    user:       user,                
                                    datauser:   request.session.user,
                                    langName:   request.session.lang,
                                    lang:       langLabels[request.session.lang]
                                });
                            });
                        }
                    });
                }
                else
                {
                    if(data.user_id != request.session.user.id && request.session.user.role =="user")                    
                        response.redirect('/admin/error?error=' + langLabels[request.session.lang].outOfBarrier);                      
                    else
                    {
                        model.readProducts(null, (err, products) => 
                        {  
                            if(request.session.user.role != "user")
                            {
                                model.readUsers(null, (err, users) => 
                                { 
                                    response.render('admin/admin_addComment', 
                                    {
                                        data:       data,
                                        products:   products,
                                        users:      users,                
                                        datauser:   request.session.user,
                                        langName:   request.session.lang,
                                        lang:       langLabels[request.session.lang]
                                    });
                                });
                            }
                            else
                            {
                                model.readUser(request.session.user.id, (err, user) => 
                                { 
                                    response.render('admin/admin_addComment', 
                                    {
                                        data:       data,
                                        products:   products,
                                        user:       user,                
                                        datauser:   request.session.user,
                                        langName:   request.session.lang,
                                        lang:       langLabels[request.session.lang]
                                    });
                                });
                            }
                        });
                    }
                }                  
            });
        });
        app.post('/admin/commt/saveComment', headerCashe, checkSignIn, (request, response) => 
        {
            if(
                request.body.product_id !== undefined && 
                request.body.datewrite !== undefined &&
                request.body.user_id !== undefined&& 
                request.body.commenttext !== undefined) 
            {
                model.saveComment(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/commt');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/commt/removeComment', headerCashe, checkSignIn, roleAdmin, (request, response) => 
        {
            model.deleteComment(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/commt');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        });
        app.get('/admin/news', headerCashe, checkSignIn, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('news', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/news');
                    else
                        response.redirect('/admin/news?page=' + countPage);
                }
                else
                {     
                    model.newsAll(page - 1, (err, data) => 
                    {
                        response.render('admin/admin_news',
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage), 
                        });
                    });
                }
            });
        });        
        app.get('/admin/categs', headerCashe, checkSignIn, (request, response) => 
        {
            var page = request.query.page == undefined ? 1 : parseInt(request.query.page);
            model.count('categorys', (err, count)=>
            {
                var countPage = count % 5 == 0 ? parseInt(count / 5) : parseInt(count / 5) + 1;
                if((page < 1 || page > countPage) && !(page == 1 && countPage == 0))
                {
                    if(page < 1)
                        response.redirect('/admin/categs');
                    else
                        response.redirect('/admin/categs?page=' + countPage);
                }
                else
                {     
                    model.readCategorys(page - 1, (err, data) => 
                    {
                        response.render('admin/admin_categorys',
                        {
                            table:      data,                
                            datauser:   request.session.user,
                            langName:   request.session.lang,
                            lang:       langLabels[request.session.lang],
                            page:       page,
                            count:      countPage == 0 ? 1 : countPage,
                            arrayList:  this.pagination(page,countPage), 
                        });
                    });
                }
            });
        }); 
        app.get('/admin/news/addNews', headerCashe, checkSignIn, (request, response) => 
        {
            model.readNews(request.query.id, (err, data) => 
            {
                if(data === null)                 
                    data = 
                    {
                        name:'',
                        photo:null,
                        definition:'',
                        datewrite:''
                    };     
                response.render('admin/admin_addNews', 
                {
                    data:       data,                
                    datauser:   request.session.user,
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            });
        });       
        app.get('/admin/categs/addCateg', headerCashe, checkSignIn, roleManager, (request, response) => 
        {
            model.readCategory(request.query.id, (err, data) => 
            {
                if(data === null)                 
                    data = 
                    {
                        name:'',
                        photo:null,
                        shortlink:''
                    };     
                response.render('admin/admin_addCateg', 
                {
                    data:       data,                
                    datauser:   request.session.user,
                    langName:   request.session.lang,
                    lang:       langLabels[request.session.lang]
                });
            });
        });
        app.post('/admin/news/saveNews', headerCashe, checkSignIn, upload.single('photo'), (request, response) => 
        {
            if(
                request.body.name !== undefined) 
            {
                if(request.file)
                    request.body.photo = request.file.filename;           
                else                
                    request.body.photo = null;
                model.saveNews(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/news');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/categs/saveCateg', headerCashe, checkSignIn, roleManager, upload.single('photo'), (request, response) => 
        {
            if(
                request.body.name !== undefined && 
                request.body.shortlink !== undefined) 
            {
                if(request.file)
                    request.body.photo = request.file.filename;           
                else                
                    request.body.photo = null;
                model.saveCategory(request.body, (err, result) => 
                {
                    if(!err)
                        response.redirect('/admin/categs');
                    else
                        response.redirect('/admin/error?error=' + err); 
                });
            } 
            else            
                response.redirect('/admin/error?error=' + langLabels[request.session.lang].notAllParam);                     
        });
        app.post('/admin/news/removeNews', headerCashe, checkSignIn, roleManager, (request, response) => 
        {
            model.deleteNews(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/news');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        }); 
        app.post('/admin/categs/removeCateg', headerCashe, checkSignIn, roleManager, (request, response) => 
        {
            model.deleteCategory(request.body.id, (err, result) =>
            {
                if(!err)
                    response.redirect('/admin/categs');
                else
                    response.redirect('/admin/error?error=' + err); 
            });
        }); 
        app.get('/admin/logout', headerCashe, checkSignIn,(request, response) =>
        {
            if(request.session.user)
            {
                request.session.destroy();
                response.redirect('/admin'); 
            } 
            else
                response.redirect('/admin');
        });  
        function headerCashe(req, res, next)
        {
            res.header("Cache-Control", "no-cache, no-store, must-revalidate"); 
            res.header("Pragma", "no-cache"); 
            res.header("Expires", 0); 
			if(!req.session.lang)
                req.session.lang = "ru";
            if(req.session.user)
            {
                model.loadDataUser(req.session.user.id, (err, data) =>
                {
                    if(!err)
                    {
                        req.session.user = 
                        {
                            id:     data.id, 
                            name:   data.name, 
                            role:   data.role                          
                        };    
                        next(); 
                    }
                    else
                        res.redirect('/admin/error?error=' + err); 
                });
            }
            else            
                next(); 
        }           
        function checkSignIn(req, res, next)
        {
            if(req.session.user)            
               next(); 
            else 
                res.redirect('/admin/login');
        }  
        function roleAdmin(req, res, next)
        {
            if(req.session.user.role == "admin")            
               next(); 
            else 
                res.redirect('/admin/error?error=' + langLabels[request.session.lang].placeOnlyAdmin);
        }  
        function roleManager(req, res, next)
        {
            if(req.session.user.role == "manager")            
               next(); 
            else 
                res.redirect('/admin/error?error=' + langLabels[request.session.lang].placeOnlyManager);
        }   
    }
};
