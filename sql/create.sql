CREATE DATABASE hardware;

CREATE SEQUENCE serial

CREATE TABLE categorys (
    id integer DEFAULT nextval('serial') NOT NULL,
    name character varying(30) NOT NULL,
    photo text,
    shortlink character varying(20) NOT NULL,
    definition text
);

CREATE TABLE news (
    id integer DEFAULT nextval('serial') NOT NULL,
    name character varying(30) NOT NULL,
    photo text,
    datewrite date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    definition text NOT NULL
);

CREATE TABLE comments (
    id integer DEFAULT nextval('serial') NOT NULL,
    product_id integer NOT NULL,
    datewrite date DEFAULT CURRENT_TIMESTAMP NOT NULL,
    user_id integer NOT NULL,
    commenttext text NOT NULL
);

CREATE TABLE deals (
    id integer DEFAULT nextval('serial') NOT NULL,
    user_id integer NOT NULL,
    product_id integer NOT NULL,
    datebuy date DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE products (
    id integer DEFAULT nextval('serial') NOT NULL,
    category_id integer NOT NULL,
    name character varying(20) NOT NULL,
    maker character varying(20) NOT NULL,
    weight real NOT NULL,
    price real NOT NULL,
    total integer NOT NULL,
    definition text,
    photo text
);

CREATE TABLE roles (
    id integer DEFAULT nextval('serial') NOT NULL,
    name character varying(100) NOT NULL
);

CREATE TABLE users (
    id integer DEFAULT nextval('serial') NOT NULL,
    email character varying(40) NOT NULL,
    firstname character varying(30) NOT NULL,
    lastname character varying(30) NOT NULL,
    dateyear date,
    city character varying(40),
    photo text,
    password text NOT NULL,
    role_id integer NOT NULL
);


INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (17, 'Процессоры', NULL, 'proc', NULL);
INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (18, 'Видеокарты', NULL, 'video', NULL);
INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (19, 'Оперативная память', NULL, 'ram', NULL);
INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (20, 'Метеринские платы', NULL, 'mather', NULL);
INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (21, 'Кулеры', NULL, 'cul', NULL);
INSERT INTO categorys (id, name, photo, shortlink, definition) VALUES (22, 'Жёсткие диски', NULL, 'hdd', NULL);

INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (29, 23, '2019-04-23', 11, 'nice site');
INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (30, 26, '2019-04-23', 12, 'nice product');
INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (31, 25, '2019-04-27', 14, 'когда появится?z');
INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (67, 28, '2019-04-20', 58, 'vcsdv');
INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (87, 28, '2019-04-28', 11, 'fghtyhjt');
INSERT INTO comments (id, product_id, datewrite, user_id, commenttext) VALUES (96, 23, '2019-05-01', 14, '');


INSERT INTO deals (id, user_id, product_id, datebuy) VALUES (71, 11, 23, '2019-04-26');
INSERT INTO deals (id, user_id, product_id, datebuy) VALUES (72, 11, 23, '2019-04-26');
INSERT INTO deals (id, user_id, product_id, datebuy) VALUES (73, 54, 28, '2019-04-26');
INSERT INTO deals (id, user_id, product_id, datebuy) VALUES (74, 11, 23, '2019-04-17');
INSERT INTO deals (id, user_id, product_id, datebuy) VALUES (80, 16, 28, '2019-04-26');

INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (23, 17, 'Intel core i7 7700k', 'Intel', 1, 299.98999, 2, NULL, NULL);
INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (24, 17, 'Intel core i7 7700', 'Intel', 1, 249.990005, 1, NULL, NULL);
INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (25, 17, 'Intel Core i9 9990XE', 'Intel', 2, 2500, 0, NULL, NULL);
INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (26, 18, 'Nvidia RTX 2080 8gb', 'nvidia', 5, 800, 1, NULL, NULL);
INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (27, 19, 'HyperX Fury 8GB DDR4', 'HyperX', 1, 200, 1, NULL, NULL);
INSERT INTO products (id, category_id, name, maker, weight, price, total, definition, photo) VALUES (28, 20, 'Gigabyte B450M DS3H', 'Gigabyte', 2, 80, 1, 'fghgdwg e wvcw w', NULL);

INSERT INTO roles (id, name) VALUES (1, 'user');
INSERT INTO roles (id, name) VALUES (0, 'admin');
INSERT INTO roles (id, name) VALUES (57, 'manager');

INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (11, 'qwerty1@mail.ru', 'Александр', 'Ефремов', NULL, NULL, NULL, '123456789', 1);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (12, 'qwerty2@mail.ru', 'Евгений', 'Ефремов', NULL, NULL, NULL, '123456789', 1);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (13, 'qwerty3@mail.ru', 'Павел', 'Ефремов', NULL, NULL, NULL, '123456789', 1);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (14, 'qwerty4@mail.ru', 'Максим', 'Ефремов', NULL, NULL, NULL, '123456789', 1);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (16, 'poka-riteli@mail.ru', 'Евгений', 'Бородин', NULL, NULL, NULL, '123456qwerty', 0);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (54, 'pppppp@mail.ru', 'Грандмастер', 'Фрилансер', '2019-04-10', 'витебск', NULL, '123123123', 0);
INSERT INTO users (id, email, firstname, lastname, dateyear, city, photo, password, role_id) VALUES (58, 'poka@mail.ru', 'Гардеев', 'Михаил', '2019-04-17', 'витебск', NULL, '123123123', 57);


ALTER TABLE ONLY categorys
    ADD CONSTRAINT categorys_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);

ALTER TABLE ONLY deals
    ADD CONSTRAINT deals_pkey PRIMARY KEY (id);

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);

ALTER TABLE ONLY categorys
    ADD CONSTRAINT uc_category UNIQUE (name);

ALTER TABLE ONLY products
    ADD CONSTRAINT uc_product UNIQUE (name);

ALTER TABLE ONLY roles
    ADD CONSTRAINT uc_role UNIQUE (name);

ALTER TABLE ONLY users
    ADD CONSTRAINT uc_user UNIQUE (email);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_comments_to_products FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE;

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_comments_to_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
	
ALTER TABLE ONLY deals
    ADD CONSTRAINT fk_deals_to_products FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE;
	
ALTER TABLE ONLY deals
    ADD CONSTRAINT fk_deals_to_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;
	
ALTER TABLE ONLY products
    ADD CONSTRAINT fk_products_to_categorys FOREIGN KEY (category_id) REFERENCES categorys(id);
	
ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_to_roles FOREIGN KEY (role_id) REFERENCES roles(id);
