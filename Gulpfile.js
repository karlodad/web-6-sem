var gulp = require('gulp');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var assets  = require('postcss-assets');
gulp.task('css', function () 
{
	var processors = [autoprefixer, cssnano,assets({loadPaths: ['images/']})];
	return gulp.src('./public/css/*.css')
		.pipe(postcss(processors))
		.pipe(gulp.dest('./public/css'));
});