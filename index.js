const   
    express             = require('express'),
    ect                 = require('ect'),
    session             = require('express-session'),

    controller          = require('./app/controller'),
    model               = require('./app/model'),
    main_controller     = require('./app/main_controller'),
    main_model          = require('./app/main_model'),

    port 				= process.env.PORT || 80, 
    app                 = express(),
    renderer            = ect({watch: true, root: __dirname + '/views', ext: 'ect'});

app.use(session({
    name: 'sess',
    secret: 'karlodad',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 5
    }
}));
app.use(require('body-parser').urlencoded({extended: true}));
app.use('/',express.static(__dirname + '/public'));
app.use('/uploads',express.static(__dirname + '/uploads'));
app.set('view engine', 'ect');
app.engine('ect', renderer.render);
app.disable('view cache');

main_controller.init(app, main_model);
controller.init(app, model);

app.listen(port,(err) => 
{
    if(err) 
        return console.log('error', err);    
    console.log('server starts');
});
